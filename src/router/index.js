import Vue from "vue";
import VueRouter from "vue-router";

// Views
import Home from "../views/Home.vue";
import Talks from "../views/Talks.vue";
import Schedule from "../views/Schedule";
import Speakers from "../views/Speakers";
import Sponsors from "../views/Sponsors";
import Contact from "../views/Contact";
import Register from "../views/Register";
import Login from "../views/Login";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Prelaunch",
    component: Home,
    meta: {
      title: "CreativeFestNOW - Stories don't stop"
    }
  },
  {
    path: "/talks",
    name: "Talks",
    component: Talks,
    meta: {
      title: "Talks - CreativeFestNOW 2020"
    }
  },
  {
    path: "/schedule",
    name: "Schedule",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Schedule,
    meta: {
      title: "Schedule - CreativeFestNOW 2020"
    }
  },
  {
    path: "/speakers",
    name: "Speakers",
    component: Speakers,
    meta: {
      title: "Speakers - CreativeFestNOW 2020"
    }
  },
  {
    path: "/sponsors",
    name: "Sponsors",
    component: Sponsors,
    meta: {
      title: "Sponsors - CreativeFestNOW 2020"
    }
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
    meta: {
      title: "Contact Us - CreativeFestNOW 2020"
    }
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      title: "Register ‹ CreativeFestNOW 2020"
    }
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      title: "Login ‹ CreativeFestNOW 2020"
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(function(to, from, next) {
  document.title = to.meta.title;
  next();
});

export default router;
